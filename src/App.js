import './App.css';
import '../node_modules/bulma/css/bulma.min.css';
import Header from './Components/Header/Header';
import Card from './Components/Card/Card';
import { useState } from 'react';
import { logDOM } from '@testing-library/react';

function App() {

  const [state, setState] = useState([
    // {task: "Faire les courses", txt:"Tous les mercredi soirs"}, 
    // {task: "Prendre mes médicaments", txt:"10h: Cortisone / Ramipril / Orocal - 19h : Omeprazol"},  
    // {task: "Lancer lave vaisselle", txt:"Tous les 2 jours"} 
  ])

  const [task, setTask] = useState();
  const [txt, setTxt] = useState();

  function createCard(e){
    e.preventDefault();
    // [...state] reprend tout ce qui a dans le tab
    const newTab = [...state, {task: task, txt: txt}]
    setState(newTab);
    setTask("");
    setTxt("");
  }

  function deleteCard(index){
    const tab = [...state];
    const tabUpdated = tab.filter(item => tab.indexOf(item) != tab.indexOf
    (tab[index]));
    setState(tabUpdated);
  }

  return (
    <div>
      <Header/>

      <div className="container px-3">
        <h2 className="title mt-5">Rentrez vos tâches à faire</h2>
        
        <form onSubmit={createCard}>

          <div className='field'>
            <div className="control">
              <label htmlFor="task" className="label">Tâche</label>
              <input 
              value={task}
              className='input'
              type="text" 
              id="task"
              placeholder='Votre tâche'
              onChange={e => setTask(e.target.value)}
            />
            </div>
          </div>

          <div className="field">
            <label htmlFor="txt" className="label">Contenu de la tâche</label>
            <textarea 
            value={txt}
            className='textarea'
            type="text" 
            id="txt"
            placeholder='Description'
            onChange={e => setTxt(e.target.value)}
            >
            </textarea>
          </div>

          <div className="field">
            <div className="control">
              <button type="submit" className="button is-dark">
                Créer une tâche
              </button>
            </div>
          </div>

        </form>

        {
          state.map((todo, index) => (
            <Card
            key={index}
            index={index}
            task={todo.task}
            txt={todo.txt} 
            deleteCard={deleteCard}>
            </Card>
          ))
        }

      </div>
    </div>
  );
}

export default App;
